﻿using NUnit.Framework;
using PascalsTriangle;
using System.Collections.Generic;

namespace PascalsTriangleUnitTests
{
    [TestFixture]
    public class UnitTests
    {
        [Test]
        public void Line_1_OfTriangle()
        {
            Assert.AreEqual(
                new List<int>() { 1 }, 
                Display.OneLine(new List<int>()));
        }

        [Test]
        public void Line_2_OfTriangle()
        {
            Assert.AreEqual(
                new List<int>() { 1, 1 }, 
                Display.OneLine(new List<int>() { 1 }));
        }

        [Test]
        public void Line_3_OfTriangle()
        {
            Assert.AreEqual(
                new List<int>() { 1, 2, 1 },
                Display.OneLine(new List<int>() { 1, 1 }));
        }

        [Test]
        public void Line_4_OfTriangle()
        {
            Assert.AreEqual(
                new List<int>() { 1, 3, 3, 1 },
                Display.OneLine(new List<int>() { 1, 2, 1 }));
        }

        [Test]
        public void Line_5_OfTriangle()
        {
            Assert.AreEqual(
                new List<int>() { 1, 4, 6, 4, 1 },
                Display.OneLine(new List<int>() { 1, 3, 3, 1 }));
        }

        [Test]
        public void Line_6_OfTriangle()
        {
            Assert.AreEqual(
                new List<int>() { 1, 5, 10, 10, 5, 1 },
                Display.OneLine(new List<int>() { 1, 4, 6, 4, 1 }));
        }
    }
}
