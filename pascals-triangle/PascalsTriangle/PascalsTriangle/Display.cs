﻿using System.Collections.Generic;

namespace PascalsTriangle
{
    public static class Display
    {
        public static List<int> OneLine(List<int> previousRow)
        {
            var thisRow = new List<int>() { 1 };

            if (previousRow == null || previousRow.Count == 0)
            {
                return thisRow;
            }

            for (int i = 0; i < previousRow.Count - 1; i++)
            {
                thisRow.Add(previousRow[i] + previousRow[i + 1]);
            }

            thisRow.Add(1);

            return thisRow;
        }
    }
}
