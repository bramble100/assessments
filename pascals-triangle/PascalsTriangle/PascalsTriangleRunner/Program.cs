﻿using System;
using System.Collections.Generic;

namespace PascalsTriangleRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfRows = 8;
            var pascalsTriangle = new List<List<int>>();
            pascalsTriangle.Add(new List<int>());

            for (int i = 1; i < numberOfRows; i++)
            {
                pascalsTriangle.Add(PascalsTriangle.Display.OneLine(pascalsTriangle[i - 1]));
            }

            for (int i = 0; i < numberOfRows; i++)
            {
                foreach (var number in pascalsTriangle[i])
                {
                    Console.Write($"{number} ");
                }            
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
