﻿using NUnit.Framework;

namespace LeapYearUnitTests
{
    [TestFixture]
    public class Tests
    {
        [TestCase(1)]
        [TestCase(2017)]
        public void NormalNotLeapYear(int year)
        {
            Assert.IsFalse(LeapYear.Calendar.IsLeapYear(year));
        }

        [TestCase(4)]
        [TestCase(2016)]
        public void NormalLeapYear(int year)
        {
            Assert.IsTrue(LeapYear.Calendar.IsLeapYear(year));
        }

        [TestCase(100)]
        [TestCase(1900)]
        public void NotLeapYear_DividableBy100_ButNotBy400(int year)
        {
            Assert.IsFalse(LeapYear.Calendar.IsLeapYear(year));
        }

        [TestCase(400)]
        [TestCase(2000)]
        public void LeapYear_DividableBy400(int year)
        {
            Assert.IsTrue(LeapYear.Calendar.IsLeapYear(year));
        }
    }
}
