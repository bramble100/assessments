﻿using System;

namespace GigaSecond
{
    public static class Calculator
    {
        public static DateTime GigaSecondMoment(DateTime dateOfBirth)
        {
            int secondsRemainedForCalculation = 1_000_000_000;
            DateTime gigaSecondMoment = new DateTime(
                dateOfBirth.Year, 
                dateOfBirth.Month,
                dateOfBirth.Day,
                dateOfBirth.Hour,
                dateOfBirth.Minute,
                dateOfBirth.Second);

            while (secondsRemainedForCalculation > 0)
            {
                gigaSecondMoment = gigaSecondMoment.AddSeconds(1);
                secondsRemainedForCalculation--;
            }

            return gigaSecondMoment;
        }
    }
}
